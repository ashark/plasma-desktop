# translation of kcminput.po to Kannada
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manu B <iammanu@gmail.com>, 2007.
# Umesh Rudrapatna <umeshrs@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-20 00:51+0000\n"
"PO-Revision-Date: 2008-01-11 11:48+0530\n"
"Last-Translator: Umesh Rudrapatna <umeshrs@gmail.com>\n"
"Language-Team: Kannada <kde-l10n-kn@kde.org>\n"
"Language: kn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"X-Generator: KBabel 1.11.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ಮನು .ಬಿ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "iammanu@gmail.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:36
#, kde-format
msgid "Pointer device KCM"
msgstr ""

#: kcm/libinput/libinput_config.cpp:38
#, kde-format
msgid "System Settings module for managing mice and trackballs."
msgstr ""

#: kcm/libinput/libinput_config.cpp:40
#, kde-format
msgid "Copyright 2018 Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43 kcm/xlib/xlib_config.cpp:91
#, kde-format
msgid "Roman Gilg"
msgstr ""

#: kcm/libinput/libinput_config.cpp:43
#, kde-format
msgid "Developer"
msgstr ""

#: kcm/libinput/libinput_config.cpp:109
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:114
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:125
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:145
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:167
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:191
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:193
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:79
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:103 kcm/libinput/main_deviceless.qml:54
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "ಸಾ(&ಮ)ಮಾನ್ಯ"

#: kcm/libinput/main.qml:105
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:124
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:130 kcm/libinput/main_deviceless.qml:56
#, kde-format
msgid "Left handed mode"
msgstr ""

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:75
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:155 kcm/libinput/main_deviceless.qml:81
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:174 kcm/libinput/main_deviceless.qml:100
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:184 kcm/libinput/main_deviceless.qml:110
#, fuzzy, kde-format
#| msgid "Pointer acceleration:"
msgid "Pointer speed:"
msgstr "ಸೂಚಿ ತ್ವರಣ:"

#: kcm/libinput/main.qml:216 kcm/libinput/main_deviceless.qml:142
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "ತ್ವರಣ (&ವ)ವೈಶಿಷ್ಟ್ಯ:"

#: kcm/libinput/main.qml:247 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:250 kcm/libinput/main_deviceless.qml:176
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:257 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:260 kcm/libinput/main_deviceless.qml:186
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:272 kcm/libinput/main_deviceless.qml:198
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:274 kcm/libinput/main_deviceless.qml:200
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "ಸುರುಳು ಧಿಕ್ಕನ್ನು ತಿರುಮುರುಗಾಗಿಸು"

#: kcm/libinput/main.qml:289 kcm/libinput/main_deviceless.qml:215
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:295
#, fuzzy, kde-format
#| msgid "Pointer acceleration:"
msgid "Scrolling speed:"
msgstr "ಸೂಚಿ ತ್ವರಣ:"

#: kcm/libinput/main.qml:343
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:349
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:360
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:396
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:427
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:431
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:448
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "ಸಂಪರ್ಕ ಗುಂಡಿಯನ್ನು ಒತ್ತಿ"

#: kcm/libinput/main.qml:449
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:478
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "ಸಾ(&ಮ)ಮಾನ್ಯ"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "ಗುಂಡಿ ಕ್ರಮ"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr "ಮೂಷಕ ಗಾಲಿಯ ಸುರುಳುವಿಕೆ ಯಾ 4ನೇ ಮತ್ತು 5ನೇ ಮೂಷಕ ಗುಂಡಿಗಳ ಧಿಕ್ಕನ್ನು ಬದಲಿಸಿ."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "ಸುರುಳು ಧಿಕ್ಕನ್ನು ತಿರುಮುರುಗಾಗಿಸು"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "ಪ್ರೌಢ"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "ಸೂಚಿ ತ್ವರಣ:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "ಜೋಡಿ ಕ್ಲಿಕ್ ಮಧ್ಯಂತರ:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "ಎಳೆತ ಪ್ರಾರಂಭ ಸಮಯ:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "ಎಳೆತ ಪ್ರಾರಂಭ ದೂರ:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr "ಮಿಲಿಸೆಕೆಂಡ್"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"ನೀವು ಮೂಷಕದಲ್ಲಿ ಕ್ಲಿಕ್ ಮಾಡಿ(ಉದಾ. ಬಹು-ಸಾಲು ಸಂಪಾದಕದಲ್ಲಿ) ಅದನ್ನು ಎಳೆತ ಪ್ರಾರಂಭ ಸಮಯದ "
"ಒಳಗಾಗಿ ಸರಿಸಿದರೆ, ಎಳೆತ ಕಾರ್ಯಾಚರಣೆಯನ್ನು ಮೊದಲುಗೊಳಿಸಲಾಗುತ್ತದೆ."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"ನೀವು ಮೂಷಕದಲ್ಲಿ ಕ್ಲಿಕ್ ಮಾಡಿ ಅದನ್ನು ಎಳೆತ ಪ್ರಾರಂಭ ದೂರದಷ್ಟಾದರೂಗಿ ಸರಿಸಿದರೆ, ಎಳೆತ "
"ಕಾರ್ಯಾಚರಣೆಯನ್ನು ಮೊದಲುಗೊಳಿಸಲಾಗುತ್ತದೆ."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "ಮೂಷಕ ಯಾನ"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "ತ್ವ&ರಣ ವಿಳಂಬ:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "ಪು&ನರಾವರ್ತನೆ ಅವಧಿ:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "ತ್ವರಣ &ಸಮಯ:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "&ಗರಿಷ್ಠ ವೇಗ:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr "ಚುಕ್ಕಿ/ಸೆಕೆಂಡು"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "ತ್ವರಣ (&ವ)ವೈಶಿಷ್ಟ್ಯ:"

#: kcm/xlib/xlib_config.cpp:79
#, kde-format
msgid "Mouse"
msgstr "ಮೂಷಕ"

#: kcm/xlib/xlib_config.cpp:83
#, fuzzy, kde-format
#| msgid "(c) 1997 - 2005 Mouse developers"
msgid "(c) 1997 - 2018 Mouse developers"
msgstr "(c) 1997 - 2005 ಮೌಸ್ ಡೆವೆಲಪರ್ಸ"

#: kcm/xlib/xlib_config.cpp:84
#, kde-format
msgid "Patrick Dowler"
msgstr "ಪ್ಯಾಟ್ರಿಕ್ ಡೌಲರ್"

#: kcm/xlib/xlib_config.cpp:85
#, kde-format
msgid "Dirk A. Mueller"
msgstr "ಡರ್ಕ ಎ. ಮ್ಯುಲ್ಲರ್"

#: kcm/xlib/xlib_config.cpp:86
#, kde-format
msgid "David Faure"
msgstr "ಡೇವಿಡ್ ಫಾವ್೯"

#: kcm/xlib/xlib_config.cpp:87
#, kde-format
msgid "Bernd Gehrmann"
msgstr "ಬರ್ನ್ಡ ಗೆಹರ್ಮನ್"

#: kcm/xlib/xlib_config.cpp:88
#, kde-format
msgid "Rik Hemsley"
msgstr "ರಿಕ್ ಹೆಮ್ಸಲೇ"

#: kcm/xlib/xlib_config.cpp:89
#, kde-format
msgid "Brad Hughes"
msgstr "ಬ್ರಾಡ್ ಯೂಜ್ಸ"

#: kcm/xlib/xlib_config.cpp:90
#, kde-format
msgid "Brad Hards"
msgstr "ಬ್ರಾಡ್ ಹಾಡ್ಸ೯"

#: kcm/xlib/xlib_config.cpp:283 kcm/xlib/xlib_config.cpp:288
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] "ಚುಕ್ಕಿ"
msgstr[1] "ಚುಕ್ಕಿಗಳು"

#: kcm/xlib/xlib_config.cpp:293
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] "ಸಾಲು"
msgstr[1] ""

#~ msgid "Ralf Nolden"
#~ msgstr "ರಾಲ್ಫ ನಾಲ್ಡೆನ್"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "ತ್ವರಣ &ಸಮಯ:"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "ತ್ವರಣ (&ವ)ವೈಶಿಷ್ಟ್ಯ:"

#~ msgid "Icons"
#~ msgstr "ಚಿಹ್ನೆಗಳು"

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "ಕಡತಗಳನ್ನು ಮತ್ತು ಕಡತಕೋಶಗಳನ್ನು ತೆರೆಯಲು ಇಬ್ಬಾರಿ(&ರ)-ಅದುಮಿ(ಮೊದಲ ಅದುಮುವಿಕೆಯಲ್ಲಿ "
#~ "ಚಿಹ್ನೆಗಳನ್ನು ಆರಿಸಿ)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "ಕಡತ ಯಾ ಕಡತಕೋಶವನ್ನು ಒಂದೇ ಕ್ಲಿಕ್ಕಲ್ಲಿ ತೆರೆಯುತ್ತದೆ ಮತ್ತು ಸಕ್ರಿಯಗೊಳಿಸುತ್ತದೆ."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "ಕಡತಗಳನ್ನು ಮತ್ತು ಕಡತಕೋಶಗಳನ್ನು ತೆರೆಯಲು ಒಂದುಬಾರಿ(&ದ)-ಅದುಮಿ"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "ನೀವು ಬಳಸಲು ಬಯಸುವ ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸವನ್ನು (ಥೀಮ್) ಆರಿಸಿ:"

#~ msgid "Name"
#~ msgstr "ಹೆಸರು"

#~ msgid "Description"
#~ msgstr "ವಿವರಣೆ"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr ""
#~ "ಈ ಬದಲಾವಣೆಗಳು ಪರಿಣಾಮ ಬೀರಲು ನೀವು ಕೆಡಿಇಯನ್ನು ಪುನರಾರಂಭಿಸಬೇಕಾಗುತ್ತದೆ (ರಿಸ್ಟಾರ್ಟ್)."

#~ msgid "Cursor Settings Changed"
#~ msgstr "ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಸಂಯೋಜನೆಗಳು ಬದಲಾಗಿವೆ"

#~ msgid "Small black"
#~ msgstr "ಚಿಕ್ಕ ಕಪ್ಪು"

#~ msgid "Small black cursors"
#~ msgstr "ಚಿಕ್ಕ ಕಪ್ಪು ಸ್ಥಳಸೂಚಕಗಳು (ಕರ್ಸರ್)"

#~ msgid "Large black"
#~ msgstr "ದೊಡ್ಡ ಕಪ್ಪು"

#~ msgid "Large black cursors"
#~ msgstr "ದೊಡ್ಡ ಕಪ್ಪು ಸ್ಥಳಸೂಚಕಗಳು (ಕರ್ಸರ್)"

#~ msgid "Small white"
#~ msgstr "ಚಿಕ್ಕ ಬಿಳಿ"

#~ msgid "Small white cursors"
#~ msgstr "ಚಿಕ್ಕ ಬಿಳಿ ಸ್ಥಳಸೂಚಕಗಳು (ಕರ್ಸರ್)"

#~ msgid "Large white"
#~ msgstr "ದೊಡ್ಡ ಬಿಳಿ"

#~ msgid "Large white cursors"
#~ msgstr "ದೊಡ್ಡ ಬಿಳಿ ಸ್ಥಳಸೂಚಕಗಳು (ಕರ್ಸರ್)"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "ಚಿಹ್ನೆಗಳ ಮೇಲಿರುವ ಸೂಚಿಯ ಆಕಾರವನ್ನು &ಬದಲಿಸಿ"

#~ msgid "A&utomatically select icons"
#~ msgstr "ಸ್ವಯಂಚಾಲಿ&ತವಾಗಿ ಚಿಹ್ನೆಗಳನ್ನು ಆರಿಸು"

#, fuzzy
#~| msgid "Dela&y:"
#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "ವಿಳಂ&ಬ:"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr "ಮಿಲಿಸೆಕೆಂಡ್"

#~ msgid "Mouse type: %1"
#~ msgstr "ಮೂಷಕ ಬಗೆ: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF ಕಾಲುವೆ (ಚಾನಲ್) 1ನ್ನು ನಿಗದಿಗೊಳಿಸಾಗಿದೆ.  ಕೊಂಡಿಯನ್ನು ಮರುಸ್ಥಾಪಿಸಲು ದಯವಿಟ್ಟು "
#~ "ಮೂಷಕದಲ್ಲಿನ ಸಂಪರ್ಕ ಗುಂಡಿಯನ್ನು ಒತ್ತಿ"

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF ಕಾಲುವೆ (ಚಾನಲ್) 2ನ್ನು ನಿಗದಿಗೊಳಿಸಾಗಿದೆ.  ಕೊಂಡಿಯನ್ನು ಮರುಸ್ಥಾಪಿಸಲು ದಯವಿಟ್ಟು "
#~ "ಮೂಷಕದಲ್ಲಿನ ಸಂಪರ್ಕ ಗುಂಡಿಯನ್ನು ಒತ್ತಿ"

#, fuzzy
#~| msgid "none"
#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "ಯಾವುದೂ ಇಲ್ಲ"

#~ msgid "Cordless Mouse"
#~ msgstr "ತಂತುರಹಿತ ಮೂಷಕ"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "ತಂತುರಹಿತ ಗಾಲಿ ಮೂಷಕ"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "ತಂತುರಹಿತ ದ್ಯುತೀಯ (ಆಪ್ಟಿಕಲ್) ಮೂಷಕ"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "ತಂತುರಹಿತ ದ್ಯುತೀಯ (ಆಪ್ಟಿಕಲ್) ಮೂಷಕ (2ch)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "ತಂತುರಹಿತ ಮೂಷಕ (2ch)"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "MX700 ತಂತುರಹಿತ ದ್ಯುತೀಯ (ಆಪ್ಟಿಕಲ್) ಮೂಷಕಕಲ್) ಮೂಷಕ"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "MX700 ತಂತುರಹಿತ ದ್ಯುತೀಯ (ಆಪ್ಟಿಕಲ್) ಮೂಷಕಕಲ್) ಮೂಷಕ (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "ಅಜ್ಞಾತ ಮೂಷಕ"

#~ msgid "Cordless Name"
#~ msgstr "ತಂತುರಹಿತ ಹೆಸರು"

#~ msgid "Sensor Resolution"
#~ msgstr "ಸಂವೇದಕ ಪೃಥಕ್ಕರಣ ಸಾಮರ್ಥ್ಯ (ರೆಸೊಲ್ಯೂಶನ್)"

#~ msgid "400 counts per inch"
#~ msgstr "400 ಗಣನೆಗಳು ಪ್ರತಿ ಅಂಗುಲಕ್ಕೆ"

#~ msgid "800 counts per inch"
#~ msgstr "800 ಗಣನೆಗಳು ಪ್ರತಿ ಅಂಗುಲಕ್ಕೆ"

#~ msgid "Battery Level"
#~ msgstr "ವಿದ್ಯುತ್ಕೋಶ ಹಂತ"

#~ msgid "RF Channel"
#~ msgstr "RF ಕಾಲುವೆ (ಚಾನಲ್)"

#~ msgid "Channel 1"
#~ msgstr "ಕಾಲುವೆ (ಚಾನಲ್) 1"

#~ msgid "Channel 2"
#~ msgstr "ಕಾಲುವೆ (ಚಾನಲ್) 2"

#, fuzzy
#~| msgid "&Cursor Theme"
#~ msgid "Cursor Theme"
#~ msgstr "ಸ್ಥಳಸೂ&ಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್)"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್) URL ಅನ್ನು ಎಳೆ ಅಥವಾ ನಮೂದಿಸು"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "ಚಿಹ್ನೆ ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್) ಕಡತಾಗಾರ %1 ಲಭ್ಯವಿಲ್ಲ."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್) ಕಡತಾಗಾರವನ್ನು ನಕಲಿಳಿಸಲಾಗುತ್ತಿಲ್ಲ; ದಯನಿಟ್ಟು "
#~ "ವಿಳಾಸ %1 ಸರಿಯಿದೆಯೇ ಎಂದು ಪರಿಶೀಲಿಸಿ."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "ಕಡತ %1 ಸರಿಯಾದ ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್) ಕಡತಾಗಾರದಂತೆ ತೋರದು."

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>ನೀವು ಬಳಕೆಯಲ್ಲಿರುವ ನಿಮ್ಮ ಪ್ರಸ್ತುತ ಪರಿಸರವಿನ್ಯಾಸವನ್ನು (ಥೀಮ್) ಅಳಿಸಿಹಾಕಲಾಗದು<br /"
#~ ">ನೀವು ಮೊದಲು ಬೇರೊಂದು ಪರಿಸರವಿನ್ಯಾಸಕ್ಕೆ (ಥೀಮ್) ಬದಲಾಯಿಸಬೇಕಾಗುತ್ತದೆ.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>ನೀವು <i>%1</i> ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸವನ್ನು (ಥೀಮ್) ತೆಗೆದುಹಾಕಲು "
#~ "ಬಯಸಿದ್ದೀರಾ?<br />ಇದು ಈ ಪರಿಸರವಿನ್ಯಾಸವು (ಥೀಮ್) ಅನುಸ್ಥಾಪಿಸಿದ ಎಲ್ಲಾ ಕಡತಗಳನ್ನು "
#~ "ಅಳಿಸಿಹಾಕುವುದು.</qt>"

#~ msgid "Confirmation"
#~ msgstr "ಖಚಿತಪಡಿಸುವಿಕೆ"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "%1 ಹೆಸರಿನ ಪರಿಸರವಿನ್ಯಾಸವು (ಥೀಮ್) ಈಗಾಗಲೇ ನಿಮ್ಮ ಚಿಹ್ನೆ ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್) "
#~ "ಕಡತಕೋಶದಲ್ಲಿ ಅಸ್ತಿತ್ವದಲ್ಲಿದೆ. ನೀವು ಅದನ್ನು ಈ ಪರಿಸರವಿನ್ಯಾಸದಿಂದ (ಥೀಮ್) ಪ್ರತಿಸ್ಥಾಪಿಸಲು "
#~ "ಬಯಸುತ್ತೀರಾ?"

#~ msgid "Overwrite Theme?"
#~ msgstr "ಪರಿಸರವಿನ್ಯಾಸವನ್ನು ತಿದ್ದಿಬರೆಯಬೇಕೆ?"

#, fuzzy
#~| msgid "Select the cursor theme you want to use:"
#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr "ನೀವು ಬಳಸಲು ಬಯಸುವ ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸವನ್ನು (ಥೀಮ್) ಆರಿಸಿ:"

#~ msgid "KDE Classic"
#~ msgstr "ಕೆಡಿಇ ಸಾಂಪ್ರದಾಯಿಕ"

#~ msgid "The default cursor theme in KDE 2 and 3"
#~ msgstr ""
#~ "ಕೆಡಿಇ 2 ಮತ್ತು 3ರಲ್ಲಿಯ ಪೂರ್ವನಿಯೋಜಿತ (ಡಿಫಾಲ್ಟ್) ಸ್ಥಳಸೂಚಕ (ಕರ್ಸರ್) ಪರಿಸರವಿನ್ಯಾಸ (ಥೀಮ್)"

#~ msgid "No description available"
#~ msgstr "ವಿವರಣೆ ಲಭ್ಯವಿಲ್ಲ"

#~ msgid "Short"
#~ msgstr "ಸಂಕ್ಷಿಪ್ತ"

#~ msgid "Long"
#~ msgstr "ಉದ್ದ"
