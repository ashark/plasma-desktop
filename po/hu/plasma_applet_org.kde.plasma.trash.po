# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Tamas Szanto <tszanto@interware.hu>, 2008.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2011, 2012, 2014.
# Balázs Úr <urbalazs@gmail.com>, 2012, 2013.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017, 2021.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.2\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-16 00:49+0000\n"
"PO-Revision-Date: 2021-09-03 14:23+0200\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#: contents/ui/main.qml:103
#, kde-format
msgctxt "a verb"
msgid "Open"
msgstr "Megnyitás"

#: contents/ui/main.qml:104
#, kde-format
msgctxt "a verb"
msgid "Empty"
msgstr "Üres"

#: contents/ui/main.qml:110
#, kde-format
msgid "Trash Settings…"
msgstr "A Kuka beállításai…"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"Empty"
msgstr ""
"Kuka\n"
"üres"

#: contents/ui/main.qml:161
#, kde-format
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] ""
"Kuka\n"
"Egy elem"
msgstr[1] ""
"Kuka\n"
" %1 elem"

#: contents/ui/main.qml:170
#, kde-format
msgid "Trash"
msgstr "Kuka"

#: contents/ui/main.qml:171
#, kde-format
msgid "Empty"
msgstr "Üres"

#: contents/ui/main.qml:171
#, kde-format
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "Egy elem"
msgstr[1] " %1 elem"

#~ msgid ""
#~ "Trash \n"
#~ " Empty"
#~ msgstr ""
#~ "Kuka\n"
#~ " üres"

#~ msgid "Empty Trash"
#~ msgstr "Kuka ürítése"

#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Biztosan ki szeretné üríteni a kukát? Minden eleme törölve lesz."

#~ msgid "Cancel"
#~ msgstr "Mégsem"

#~ msgid "&Empty Trashcan"
#~ msgstr "A Kuka &kiürítése"

#~ msgid "&Menu"
#~ msgstr "&Menü"

#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Kuka ürítése"

#~ msgid "Emptying Trashcan..."
#~ msgstr "A Kuka ürítése…"
