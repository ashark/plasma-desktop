# translation of kcmaccess.po to Hebrew
# KDE Hebrew Localization Project
# Translation of kcmaccess.po into Hebrew
#
# In addition to the copyright owners of the program
# which this translation accompanies, this translation is
# Copyright (C) 2000-2002 Meni Livne <livne@kde.org>
#
# This translation is subject to the same Open Source
# license as the program which it accompanies.
#
# Dror Levin <spatz@012.net.il>, 2003.
# galion <galion.lum@gmail.com>, 2005.
# Diego Iastrubni <elcuco@kde.org>, 2007, 2008, 2012.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: kcmaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:47+0000\n"
"PO-Revision-Date: 2017-05-16 12:02-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Zanata 3.9.6\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kcmaccess.cpp:116
#, kde-format
msgid "AltGraph"
msgstr "AltGraph"

#: kcmaccess.cpp:118
#, kde-format
msgid "Hyper"
msgstr "Hyper"

#: kcmaccess.cpp:120
#, kde-format
msgid "Super"
msgstr "Super"

#: kcmaccess.cpp:130
#, kde-format
msgid "Press %1 while NumLock, CapsLock and ScrollLock are active"
msgstr "לחץ על %1 בזמן ש־NumLock, CapsLock ו־ScrollLock פעילים."

#: kcmaccess.cpp:131
#, kde-format
msgid "Press %1 while CapsLock and ScrollLock are active"
msgstr "לחץ על %1 בזמן ש־CapsLock ו־ScrollLock פעילים."

#: kcmaccess.cpp:132
#, kde-format
msgid "Press %1 while NumLock and ScrollLock are active"
msgstr "לחץ על %1 בזמן ש־NumLock ו־ScrollLock פעילים."

#: kcmaccess.cpp:133
#, kde-format
msgid "Press %1 while ScrollLock is active"
msgstr "לחץ על %1 בזמן ש־ScrollLock פעיל."

#: kcmaccess.cpp:134
#, kde-format
msgid "Press %1 while NumLock and CapsLock are active"
msgstr "לחץ על %1 בזמן ש־NumLock ו־CapsLock פעילים."

#: kcmaccess.cpp:135
#, kde-format
msgid "Press %1 while CapsLock is active"
msgstr "לחץ על %1 בזמן ש־CapsLock פעיל."

#: kcmaccess.cpp:136
#, kde-format
msgid "Press %1 while NumLock is active"
msgstr "לחץ על %1 בזמן ש־NumLock פעיל."

#: kcmaccess.cpp:137
#, kde-format
msgid "Press %1"
msgstr "לחץ על %1"

#: kcmaccess.cpp:182
#, kde-format
msgid "Could not set gsettings for Orca: \"%1\" failed"
msgstr ""

#: kcmaccess.cpp:189
#, kde-format
msgid "Error: Could not launch \"orca --setup\""
msgstr ""

#. i18n: ectx: label, entry (SystemBell), group (Bell)
#: kcmaccessibilitybell.kcfg:9
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Use System Bell"
msgstr "השתמש בפעמון המער&כת"

#. i18n: ectx: label, entry (CustomBell), group (Bell)
#: kcmaccessibilitybell.kcfg:13
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Customize system bell"
msgstr "השתמש בפעמון המער&כת"

#. i18n: ectx: label, entry (CustomBellFile), group (Bell)
#: kcmaccessibilitybell.kcfg:17
#, kde-format
msgid "Sound file for the Bell"
msgstr ""

#. i18n: ectx: label, entry (VisibleBell), group (Bell)
#: kcmaccessibilitybell.kcfg:20
#, kde-format
msgid "Use a visual bell instead of a sound"
msgstr ""

#. i18n: ectx: label, entry (InvertScreen), group (Bell)
#. i18n: ectx: label, entry (AccessXTimeoutDelay), group (Keyboard)
#: kcmaccessibilitybell.kcfg:24 kcmaccessibilitykeyboard.kcfg:37
#, kde-format
msgid "Invert the system colors on the bell"
msgstr ""

#. i18n: ectx: label, entry (VisibleBellColor), group (Bell)
#: kcmaccessibilitybell.kcfg:28
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Color of the Visible Bell"
msgstr "השתמש בפעמון המער&כת"

#. i18n: ectx: label, entry (VisibleBellPause), group (Bell)
#: kcmaccessibilitybell.kcfg:32
#, kde-format
msgid "Duration of the Visible Bell"
msgstr ""

#. i18n: ectx: label, entry (StickyKeys), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:9
#, fuzzy, kde-format
#| msgid "Use &sticky keys"
msgid "Use sticky Keys"
msgstr "השתמש במקשים &דביקים"

#. i18n: ectx: label, entry (StickyKeysLatch), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:13
#, fuzzy, kde-format
#| msgid "&Lock sticky keys"
msgid "Lock the sticky keys"
msgstr "&נעל מקשים דביקים"

#. i18n: ectx: label, entry (StickyKeysAutoOff), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:17
#, kde-format
msgid "Turn sticky keys off when two keys are pressed simultaneously"
msgstr "כבה מקשים דביקים כאשר שני מקשים נלחצים בו־זמנית"

#. i18n: ectx: label, entry (StickyKeysBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:21
#, fuzzy, kde-format
#| msgid "Use &sticky keys"
msgid "Beep on a sticky key press"
msgstr "השתמש במקשים &דביקים"

#. i18n: ectx: label, entry (ToggleKeysBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:25
#, kde-format
msgid "Toggle keys beep"
msgstr ""

#. i18n: ectx: label, entry (KeyboardNotifyModifiers), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:29
#, kde-format
msgid "Notify when a keyboard modifier is pressed"
msgstr ""

#. i18n: ectx: label, entry (AccessXTimeout), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:33
#, kde-format
msgid "Use a timeout delay"
msgstr ""

#. i18n: ectx: label, entry (AccessXBeep), group (Keyboard)
#: kcmaccessibilitykeyboard.kcfg:41
#, kde-format
msgid "Use a beep for access"
msgstr ""

#. i18n: ectx: label, entry (SlowKeys), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:9
#, fuzzy, kde-format
#| msgid "&Use slow keys"
msgid "Use slow keypresses"
msgstr "השתמש במקשים אי&טיים"

#. i18n: ectx: label, entry (SlowKeysDelay), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:13
#, kde-format
msgid "Delay for the key press"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysPressBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:17
#, kde-format
msgid "Beep on a slow keypress"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysAcceptBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:21
#, kde-format
msgid "Beep on an accepted keypress"
msgstr ""

#. i18n: ectx: label, entry (SlowKeysRejectBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:25
#, kde-format
msgid "Beep on a rejected keypress"
msgstr ""

#. i18n: ectx: label, entry (BounceKeys), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:29
#, kde-format
msgid "Bounce Keys"
msgstr "מקשי הקפצה"

#. i18n: ectx: label, entry (BounceKeysDelay), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:33
#, kde-format
msgid "Use a delay for bouncing"
msgstr ""

#. i18n: ectx: label, entry (BounceKeysRejectBeep), group (Keyboard)
#: kcmaccessibilitykeyboardfilters.kcfg:37
#, kde-format
msgid "Beep if a bounce key event is rejected"
msgstr ""

#. i18n: ectx: label, entry (MouseKeys), group (Mouse)
#: kcmaccessibilitymouse.kcfg:9
#, kde-format
msgid "Use keys to control the mouse"
msgstr ""

#. i18n: ectx: label, entry (AccelerationDelay), group (Mouse)
#: kcmaccessibilitymouse.kcfg:13
#, kde-format
msgid "Delay for the mouse movement"
msgstr ""

#. i18n: ectx: label, entry (RepetitionInterval), group (Mouse)
#: kcmaccessibilitymouse.kcfg:17
#, kde-format
msgid "Repetition interval for the mouse movement"
msgstr ""

#. i18n: ectx: label, entry (MK_TimeToMax), group (Mouse)
#. i18n: ectx: label, entry (AccelerationTime), group (Mouse)
#: kcmaccessibilitymouse.kcfg:21 kcmaccessibilitymouse.kcfg:25
#, kde-format
msgid "Time to hit maximum velocity"
msgstr ""

#. i18n: ectx: label, entry (MK_MaxSpeed), group (Mouse)
#: kcmaccessibilitymouse.kcfg:28
#, kde-format
msgid "Maximum velocity"
msgstr ""

#. i18n: ectx: label, entry (MaxSpeed), group (Mouse)
#: kcmaccessibilitymouse.kcfg:32
#, kde-format
msgid "Maximum Velocity"
msgstr ""

#. i18n: ectx: label, entry (ProfileCurve), group (Mouse)
#: kcmaccessibilitymouse.kcfg:35
#, kde-format
msgid "Mouse Key Curve"
msgstr ""

#. i18n: ectx: label, entry (Gestures), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:41
#, kde-format
msgid "Use Gestures"
msgstr ""

#. i18n: ectx: label, entry (GestureConfirmation), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:45
#, kde-format
msgid "Ask for Confirmation for a gesture"
msgstr ""

#. i18n: ectx: label, entry (KeyboardNotifyAccess), group (Keyboard)
#: kcmaccessibilitymouse.kcfg:49
#, kde-format
msgid "Confirm access to the Keyboard"
msgstr ""

#. i18n: ectx: label, entry (Enabled), group (ScreenReader)
#: kcmaccessibilityscreenreader.kcfg:9 package/contents/ui/ScreenReader.qml:17
#, fuzzy, kde-format
#| msgid "Screen Reader"
msgid "Enable Screen Reader"
msgstr "קורא  מסך"

#: package/contents/ui/Bell.qml:20
#, kde-format
msgid "Please choose an audio file"
msgstr ""

#: package/contents/ui/Bell.qml:22
#, kde-format
msgid "Audio Files (*.mp3 *.ogg *.wav)"
msgstr ""

#: package/contents/ui/Bell.qml:31
#, fuzzy, kde-format
#| msgid "Audible Bell"
msgid "Audible bell:"
msgstr "פעמון נשמע"

#: package/contents/ui/Bell.qml:32
#, kde-format
msgctxt "Enable the system bell"
msgid "Enable"
msgstr ""

#: package/contents/ui/Bell.qml:43
#, kde-format
msgctxt "Defines if the system will use a sound system bell"
msgid "Custom sound:"
msgstr ""

#: package/contents/ui/Bell.qml:76
#, kde-format
msgid "Search audio file for the system bell"
msgstr ""

#: package/contents/ui/Bell.qml:77
#, kde-format
msgid "Button search audio file"
msgstr ""

#: package/contents/ui/Bell.qml:93
#, fuzzy, kde-format
#| msgid "Visible Bell"
msgid "Visual bell:"
msgstr "פעמון חזותי"

#: package/contents/ui/Bell.qml:94
#, kde-format
msgctxt "Enable visual bell"
msgid "Enable"
msgstr ""

#: package/contents/ui/Bell.qml:108
#, fuzzy, kde-format
#| msgid "I&nvert screen"
msgctxt "Invert screen on a system bell"
msgid "Invert Screen"
msgstr "הפוך את &צבעי המסך"

#: package/contents/ui/Bell.qml:125
#, kde-format
msgctxt "Flash screen on a system bell"
msgid "Flash"
msgstr ""

#: package/contents/ui/Bell.qml:136
#, kde-format
msgctxt "Color of the system bell"
msgid "Color"
msgstr ""

#: package/contents/ui/Bell.qml:147
#, fuzzy, kde-format
#| msgid "&Duration:"
msgctxt "Duration of the system bell"
msgid "Duration:"
msgstr "משך:"

#: package/contents/ui/KeyboardFilters.qml:18
#, fuzzy, kde-format
#| msgid "&Use slow keys"
msgid "Slow keys:"
msgstr "השתמש במקשים אי&טיים"

#: package/contents/ui/KeyboardFilters.qml:19
#, kde-format
msgctxt "Enable slow keys"
msgid "Enable"
msgstr ""

#: package/contents/ui/KeyboardFilters.qml:33
#, kde-format
msgctxt "Slow keys Delay"
msgid "Delay:"
msgstr ""

#: package/contents/ui/KeyboardFilters.qml:50
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Ring system bell:"
msgstr "השתמש בפעמון המער&כת"

#: package/contents/ui/KeyboardFilters.qml:51
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is pressed"
msgctxt "Use system bell when a key is pressed"
msgid "when any key is &pressed"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש נלחץ"

#: package/contents/ui/KeyboardFilters.qml:65
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is accepted"
msgctxt "Use system bell when a key is accepted"
msgid "when any key is &accepted"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש מתקבל"

#: package/contents/ui/KeyboardFilters.qml:79
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is rejected"
msgctxt "Use system bell when a key is rejected"
msgid "when any key is &rejected"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש נדחה"

#: package/contents/ui/KeyboardFilters.qml:96
#, fuzzy, kde-format
#| msgid "Bounce Keys"
msgid "Bounce keys:"
msgstr "מקשי הקפצה"

#: package/contents/ui/KeyboardFilters.qml:97
#, kde-format
msgctxt "Bounce keys enable"
msgid "Enable"
msgstr ""

#: package/contents/ui/KeyboardFilters.qml:111
#, kde-format
msgctxt "Bounce keys delay"
msgid "Delay:"
msgstr ""

#: package/contents/ui/KeyboardFilters.qml:126
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is rejected"
msgid "Ring system bell when rejected"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש נדחה"

#: package/contents/ui/main.qml:21
#, kde-format
msgid ""
"This module lets you configure the accessibility features such as a screen "
"reader."
msgstr ""

#: package/contents/ui/main.qml:26
#, fuzzy, kde-format
#| msgid "&Bell"
msgctxt "System Bell"
msgid "Bell"
msgstr "פע&מון"

#: package/contents/ui/main.qml:31
#, fuzzy, kde-format
#| msgid "Modifier Keys"
msgctxt "System Modifier Keys"
msgid "Modifier Keys"
msgstr "&מקשי שינוי"

#: package/contents/ui/main.qml:36
#, fuzzy, kde-format
#| msgid "&Keyboard Filters"
msgctxt "System keyboard filters"
msgid "Keyboard Filters"
msgstr "&מסנני מקלדת"

#: package/contents/ui/main.qml:41
#, kde-format
msgctxt "System mouse navigation"
msgid "Mouse Navigation"
msgstr ""

#: package/contents/ui/main.qml:46
#, fuzzy, kde-format
#| msgid "Screen Reader"
msgctxt "System mouse navigation"
msgid "Screen Reader"
msgstr "קורא  מסך"

#: package/contents/ui/ModifierKeys.qml:16
#, fuzzy, kde-format
#| msgid "Sticky Keys"
msgid "Sticky keys:"
msgstr "מקשי&ם דביקים"

#: package/contents/ui/ModifierKeys.qml:17
#, kde-format
msgctxt "Enable sticky keys"
msgid "Enable"
msgstr ""

#: package/contents/ui/ModifierKeys.qml:28
#, kde-format
msgctxt "Lock sticky keys"
msgid "Lock"
msgstr ""

#: package/contents/ui/ModifierKeys.qml:42
#, kde-format
msgid "Disable when two keys are held down"
msgstr ""

#: package/contents/ui/ModifierKeys.qml:54
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is pressed"
msgid "Ring system bell when modifier keys are used"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש נלחץ"

#: package/contents/ui/ModifierKeys.qml:71
#, kde-format
msgid "Feedback:"
msgstr ""

#: package/contents/ui/ModifierKeys.qml:72
#, fuzzy, kde-format
#| msgid "&Use system bell whenever a key is accepted"
msgid "Ring system bell when locking keys are toggled"
msgstr "&השתמש בפעמון המערכת בכל פעם שמקש מתקבל"

#: package/contents/ui/ModifierKeys.qml:83
#, fuzzy, kde-format
#| msgid ""
#| "Use Plasma's notification mechanism for modifier or locking key state "
#| "changes"
msgid "Show notification when modifier or locking keys are used"
msgstr "השתמש במנגנון ההתראה של KDE בכל פעם שמקש נעילה משנה את מצבו"

#: package/contents/ui/ModifierKeys.qml:95
#, fuzzy, kde-format
#| msgid "Configure &Notifications..."
msgid "Configure Notifications…"
msgstr "הגדר &התראת מערכת..."

#: package/contents/ui/MouseNavigation.qml:17
#, kde-format
msgid "Use number pad to move cursor:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:18
#, kde-format
msgid "Enable"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:29
#, kde-format
msgid "When a gesture is used:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:30
#, kde-format
msgid "Display a confirmation dialog"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:41
#, fuzzy, kde-format
#| msgid "Use &system bell"
msgid "Ring the System Bell"
msgstr "השתמש בפעמון המער&כת"

#: package/contents/ui/MouseNavigation.qml:52
#, fuzzy, kde-format
#| msgid "Notification"
msgid "Show a notification"
msgstr "התראה"

#: package/contents/ui/MouseNavigation.qml:68
#, kde-format
msgid "Acceleration delay:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:79
#, kde-format
msgid "Repeat interval:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:90
#, kde-format
msgid "Acceleration time:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:101
#, kde-format
msgid "Maximum speed:"
msgstr ""

#: package/contents/ui/MouseNavigation.qml:112
#, kde-format
msgid "Acceleration profile:"
msgstr ""

#: package/contents/ui/ScreenReader.qml:28
#, kde-format
msgid "Launch Orca Screen Reader Configuration…"
msgstr ""

#: package/contents/ui/ScreenReader.qml:41
#, fuzzy, kde-format
#| msgid ""
#| "Please note that you may have to log out once to allow the screen reader "
#| "to work properly."
msgid ""
"Please note that you may have to log out or reboot once to allow the screen "
"reader to work properly."
msgstr "שים לב שאולי תצטרך להתחבר שוב כדי שהקורא מסך יפעל כראוי."

#: package/contents/ui/ScreenReader.qml:42
#, kde-format
msgid ""
"It appears that the Orca Screen Reader is not installed. Please install it "
"before trying to use this feature, and then log out or reboot"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "דרור לוין, גל לומברוזו"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "spatz@012.net.il, galion.lum@gmail.com"

#, fuzzy
#~| msgid "KDE Accessibility Tool"
#~ msgid "Accessibility"
#~ msgstr "כלי הנגישות של KDE"

#~ msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
#~ msgstr "‎(c) 2000, Matthias Hoelzer-Kluepfel"

#~ msgid "Matthias Hoelzer-Kluepfel"
#~ msgstr "Matthias Hoelzer-Kluepfel"

#~ msgid "Author"
#~ msgstr "כותב"

#, fuzzy
#~| msgid "Activation Gestures"
#~ msgid "Activation:"
#~ msgstr "מנחות הפעלה"

#~ msgid ""
#~ "If this option is checked, the default system bell will be used. Usually, "
#~ "this is just a \"beep\"."
#~ msgstr ""
#~ "אם אפשרות זו נבחרת, ייעשה שימוש בברירת המחדל של פעמון המערכת. בדרך כלל, "
#~ "זה רק \"ביפ\"."

#~ msgid ""
#~ "Check this option if you want to use a customized bell, playing a sound "
#~ "file. If you do this, you will probably want to turn off the system bell."
#~ msgstr ""
#~ "בחר באפשרות זו אם ברצונך בפעמון מותאם אישית שינגן קובץ צליל. אם אתה עושה "
#~ "את זה, ודאי תרצה לכבות את פעמון המערכת."

#~ msgid "Us&e customized bell"
#~ msgstr "השתמש בפעמון מותא&ם אישית"

#~ msgid ""
#~ "If the option \"Use customized bell\" is enabled, you can choose a sound "
#~ "file here. Click \"Browse...\" to choose a sound file using the file "
#~ "dialog."
#~ msgstr ""
#~ "אם האפשרות \"השתמש בפעמון מותאם אישית\" נבחרה, באפשרותך לבחור כאן קובץ "
#~ "צליל. לחץ על \"עיון...\" כדי לבחור קובץ צליל בעזרת דו-שיח הקבצים."

#~ msgid "Sound &to play:"
#~ msgstr "צליל ל&ניגון:"

#~ msgid "Browse..."
#~ msgstr "עיין..."

#, fuzzy
#~| msgid ""
#~| "This option will turn on the \"visible bell\", i.e. a visible "
#~| "notification shown every time that normally just a bell would occur. "
#~| "This is especially useful for deaf people."
#~ msgid ""
#~ "This option will turn on the \"visual bell\", i.e. a visual notification "
#~ "shown every time that normally just a bell would occur. This is "
#~ "especially useful for deaf people."
#~ msgstr ""
#~ "אפשרות זו תפעיל את \"הפעמון החזותי\", הודעה חזותית שתוצג בכל פעם שאמור "
#~ "היה להתרחש צפצוף פעמון בלבד. דבר זה שימושי במיוחד לאנשים חירשים."

#, fuzzy
#~| msgid "&Use visible bell"
#~ msgid "&Use visual bell"
#~ msgstr "השתמש בפעמ&ון חזותי"

#~ msgid ""
#~ "The screen will turn to a custom color for the amount of time specified "
#~ "below."
#~ msgstr "המסך ישנה את צבעו לצבע מותאם אישית למשך הזמן המצויין להלן."

#~ msgid "F&lash screen"
#~ msgstr "הבזק את המ&סך"

#~ msgid ""
#~ "All screen colors will be inverted for the amount of time specified below."
#~ msgstr "כל צבעי המסך יתהפכו למשך הזמן המצויין להלן."

#~ msgid ""
#~ "Click here to choose the color used for the \"flash screen\" visible bell."
#~ msgstr ""
#~ "לחץ כאן כדי לבחור את הצבע שנעשה בו שימוש לפעמון החזותי של \"הבזק את המסך"
#~ "\"."

#~ msgid ""
#~ "Here you can customize the duration of the \"visible bell\" effect being "
#~ "shown."
#~ msgstr "כאן באפשרותך להתאים אישית את משך אפקט \"הפעמון החזותי\" המוצג."

#~ msgid " msec"
#~ msgstr " אלפיות שנייה"

#~ msgid "Use system bell whenever a modifier gets latched, locked or unlocked"
#~ msgstr "השתמשת בפעמון המערכת כאשר מקש נעילה מופעל, מכונה או משנה את מצבו."

#~ msgid "Locking Keys"
#~ msgstr "מקשי נעילה"

#~ msgid "Use system bell whenever a locking key gets activated or deactivated"
#~ msgstr "השתמש בפעמון המערכת בכל פעם שמקש נעילה הופך פעיל או כבוי."

#~ msgid "Slo&w Keys"
#~ msgstr "מקשים אי&טיים"

#~ msgid "Use bou&nce keys"
#~ msgstr "השתמש במקשי הקפ&צה"

#~ msgid "Use the system bell whenever a key is rejected"
#~ msgstr "השתמש בפעמון המערכת בכל פעם שמקש נדחה"

#, fuzzy
#~| msgid "Activation Gestures"
#~ msgid "Activation &Gestures"
#~ msgstr "מנחות הפעלה"

#~ msgid "Use gestures for activating sticky keys and slow keys"
#~ msgstr "השתמש במנחות להפעלת מקשים דביקים ומקשים איטיים "

#~ msgid ""
#~ "Turn sticky keys and slow keys off after a certain period of inactivity."
#~ msgstr "כבה מקשים איטיים ומקשים דביקים אחרי זמן מסוים של אי־פעילות."

#~ msgid " min"
#~ msgstr " דקות"

#~ msgid ""
#~ "Use the system bell whenever a gesture is used to toggle an accessibility "
#~ "feature"
#~ msgstr ""
#~ "השתמש בפעמון המערכת בכל פעם שמשתמשים במחווה כדי להדליק או לכבות אפשרות "
#~ "נגישות"

#~ msgid ""
#~ "If this option is checked, KDE will show a confirmation dialog whenever a "
#~ "keyboard accessibility feature is turned on or off.\n"
#~ "Ensure you know what you are doing if you uncheck it, as the keyboard "
#~ "accessibility settings will then always be applied without confirmation."
#~ msgstr ""
#~ "אם אפשרות זו מופעלת, KDE יציג דו־שיח לאישור בכל פעם שמכבים או מדליקים "
#~ "אפשרות נגישות מקלדת.\n"
#~ "שימו לב! אם אפשרות זו כבויה, מנגנוני הנגשת המקלדת יופעלו להבא ללא אישורכם."

#~ msgid ""
#~ "Show a confirmation dialog whenever a keyboard accessibility feature is "
#~ "toggled"
#~ msgstr ""
#~ "הראה תיבת דו-שיח לאישור בכל פעם שמכבים או מדליקים אפשרות נגישות מקלדת"

#~ msgid ""
#~ "Use Plasma's notification mechanism whenever a keyboard accessibility "
#~ "feature is toggled"
#~ msgstr "בכל פעם שמצב אפשרויות נגישות מקלדת משתנה הראה התראה"

#~ msgid "Screen reader enabled"
#~ msgstr "קורא מסך מאופשר"

#~ msgid ""
#~ "Here you can activate keyboard gestures that turn on the following "
#~ "features: \n"
#~ "Sticky keys: Press Shift key 5 consecutive times\n"
#~ "Slow keys: Hold down Shift for 8 seconds"
#~ msgstr ""
#~ "כאן אפשר להפעיל את מנחות המקלדת שמפעילות את האפשרויות הבאות:\n"
#~ "מקשים דביקים: לחץ על Shift 5 פעמים רצופות\n"
#~ "מקשים איטיים: לחץ על Shift למשך 8 שניות"

#~ msgid ""
#~ "Here you can activate keyboard gestures that turn on the following "
#~ "features: \n"
#~ "Mouse Keys: %1\n"
#~ "Sticky keys: Press Shift key 5 consecutive times\n"
#~ "Slow keys: Hold down Shift for 8 seconds"
#~ msgstr ""
#~ "כאן אפשר להפעיל את מנחות המקלדת שמפעילות את האפשרויות הבאות:\n"
#~ "מקשי עכבר: %1\n"
#~ "מקשים דביקים: לחץ על Shift 5 פעמים רצופות\n"
#~ "מקשים איטיים: לחץ על Shift למשך 8 שניות"

#~ msgid "&Activation Gestures"
#~ msgstr "מ&חוות הפעלה"
