# translation of kcm_splashscreen.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2015.
# Mthw <jari_45@hotmail.com>, 2018, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019, 2020, 2021.
# Dušan Kazik <prescott66@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcm_splashscreen\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-10 00:47+0000\n"
"PO-Revision-Date: 2021-06-26 19:40+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: kcm.cpp:160
#, kde-format
msgid "None"
msgstr "Žiadna"

#: kcm.cpp:162
#, kde-format
msgid "No splash screen will be shown"
msgstr "Nezobrazí sa žiadna úvodná obrazovka"

#: kcm.cpp:180
#, kde-format
msgid "You cannot delete the currently selected splash screen"
msgstr "Aktuálne vybranú úvodnú obrazovku nemôžete odstrániť"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "This module lets you choose the splash screen theme."
msgstr "Tento modul vám umožňuje nastaviť tému úvodnej obrazovky."

#: package/contents/ui/main.qml:39
#, kde-format
msgid "Failed to show the splash screen preview."
msgstr "Nepodarilo sa zobraziť ukážku úvodnej obrazovky."

#: package/contents/ui/main.qml:71
#, kde-format
msgid "Preview Splash Screen"
msgstr "Zobraziť náhľad úvodnej obrazovky"

#: package/contents/ui/main.qml:76
#, kde-format
msgid "Uninstall"
msgstr "Odinštalovať"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "&Get New Splash Screens…"
msgstr "&Získať nové úvodné obrazovky..."

#. i18n: ectx: label, entry (engine), group (KSplash)
#: splashscreensettings.kcfg:12
#, kde-format
msgid "For future use"
msgstr "Pre budúce použitie"

#. i18n: ectx: label, entry (theme), group (KSplash)
#: splashscreensettings.kcfg:16
#, kde-format
msgid "Name of the current splash theme"
msgstr "Názov aktuálnej úvodnej témy"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Roman Paholík,Dušan Kazik"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wizzardsk@gmail.com,prescott66@gmail.com"

#~ msgid "Splash Screen"
#~ msgstr "Úvodná obrazovka"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#, fuzzy
#~| msgid "&Get New Splash Screens..."
#~ msgid "Get New Icons..."
#~ msgstr "Získať nové úvodné obrazovky..."

#, fuzzy
#~| msgid "Splash Screen"
#~ msgid "Splash Screens"
#~ msgstr "Úvodná obrazovka"

#~ msgid "Error"
#~ msgstr "Chyba"

#~ msgid "Choose the splash screen theme"
#~ msgstr "Nastaviť tému úvodnej obrazovky"

#~ msgid "Configure Splash screen details"
#~ msgstr "Nastaviť podrobnosti úvodnej obrazovky"
