# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-23 00:48+0000\n"
"PO-Revision-Date: 2021-10-07 22:48+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.2\n"

#: globalaccelmodel.cpp:113
#, kde-format
msgid "Applications"
msgstr "Applicationes"

#: globalaccelmodel.cpp:113 globalaccelmodel.cpp:274
#, kde-format
msgid "System Services"
msgstr "Servicios de systema"

#: globalaccelmodel.cpp:187
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Error durante que il salveguardava %1: %2"

#: globalaccelmodel.cpp:295
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Error durante que on addeva %1, il appare que il ha necun actiones."

#: globalaccelmodel.cpp:338
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Error durante que il communicava con le servicio de vias breve global"

#: kcm_keys.cpp:50
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Il falleva a communicar con le demone de vias breve global"

#: kcm_keys.cpp:224 package/contents/ui/main.qml:129
#: standardshortcutsmodel.cpp:29
#, kde-format
msgid "Common Actions"
msgstr "Actiones commun"

#: kcm_keys.cpp:230
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Via breve %1 es ja assignate al action commun %2 '%3'.\n"
"Tu vole reassignar lo?"

#: kcm_keys.cpp:234
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Via breve %1 es ja assignate al action commun ?%2' de  %3 \n"
"Tu vole reassignar lo?"

#: kcm_keys.cpp:235
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Trovate conflicto"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Non pote exportar schema quando on ha modificationes non salveguardate"

#: package/contents/ui/main.qml:53
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"Selige le componentes a basso que deberea esser includite in le schema "
"exportate"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "Save scheme"
msgstr "Salveguarda schema"

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Remover omne vias breve per %1"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Undo deletion"
msgstr "Annulla deletion"

#: package/contents/ui/main.qml:198
#, kde-format
msgid "No items matched the search terms"
msgstr "Necun terminos coincideva con le terminos de cerca"

#: package/contents/ui/main.qml:224
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Seliger un elemento ex le lista de vider su vias breve hic"

#: package/contents/ui/main.qml:232
#, kde-format
msgid "Add Application…"
msgstr "Adde Application..."

#: package/contents/ui/main.qml:242
#, kde-format
msgid "Import Scheme…"
msgstr "Importa schema..."

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Cancel Export"
msgstr "Cancella exportation"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Export Scheme…"
msgstr "Exporta Schema ..."

#: package/contents/ui/main.qml:268
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Exportar schema de via breve"

#: package/contents/ui/main.qml:268 package/contents/ui/main.qml:291
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Importar schema de via breve"

#: package/contents/ui/main.qml:270
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Schema de via breve (*.kksrc)"

#: package/contents/ui/main.qml:296
#, kde-format
msgid "Select the scheme to import:"
msgstr "Selige le schema de importar:"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Custom Scheme"
msgstr "Personalisar schema"

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Select File…"
msgstr "Selige File…"

#: package/contents/ui/main.qml:313
#, kde-format
msgid "Import"
msgstr "Importa"

#: package/contents/ui/ShortcutActionDelegate.qml:26
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Editante via breve: %1"

#: package/contents/ui/ShortcutActionDelegate.qml:38
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ShortcutActionDelegate.qml:51
#, kde-format
msgid "No active shortcuts"
msgstr "Necun vias breve active"

#: package/contents/ui/ShortcutActionDelegate.qml:90
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Via breve predefinite"
msgstr[1] "Vias breve predefinite"

#: package/contents/ui/ShortcutActionDelegate.qml:92
#, kde-format
msgid "No default shortcuts"
msgstr "Nulle vias breve predefinite"

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Via breve predefinite %1 es habilitate."

#: package/contents/ui/ShortcutActionDelegate.qml:100
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Via breve predefinite %1 es dishabilitate."

#: package/contents/ui/ShortcutActionDelegate.qml:121
#, kde-format
msgid "Custom shortcuts"
msgstr "Vias breve adaptabile"

#: package/contents/ui/ShortcutActionDelegate.qml:145
#, kde-format
msgid "Delete this shortcut"
msgstr "Deler iste via breve"

#: package/contents/ui/ShortcutActionDelegate.qml:151
#, kde-format
msgid "Add custom shortcut"
msgstr "Adder via breve personalisate"

#: package/contents/ui/ShortcutActionDelegate.qml:185
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Cancellar le captura de nove via breve"

#: standardshortcutsmodel.cpp:33
#, kde-format
msgid "File"
msgstr "File"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: standardshortcutsmodel.cpp:36
#, kde-format
msgid "Navigation"
msgstr "Navigation"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "View"
msgstr "Vista"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "Settings"
msgstr "Preferentias"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Help"
msgstr "Adjuta"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "Shortcuts"
#~ msgstr "Vias breve"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
