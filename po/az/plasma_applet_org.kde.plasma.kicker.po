# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-30 00:47+0000\n"
"PO-Revision-Date: 2022-07-06 17:49+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Əsas"

#: package/contents/ui/code/tools.js:42
#, kde-format
msgid "Remove from Favorites"
msgstr "Seçilmişlərdən silmək"

#: package/contents/ui/code/tools.js:46
#, kde-format
msgid "Add to Favorites"
msgstr "Seçilmişlərə əlavə etmək"

#: package/contents/ui/code/tools.js:70
#, kde-format
msgid "On All Activities"
msgstr "Bütün İş otaqlarında"

#: package/contents/ui/code/tools.js:120
#, kde-format
msgid "On the Current Activity"
msgstr "Cari İş otağında"

#: package/contents/ui/code/tools.js:134
#, kde-format
msgid "Show in Favorites"
msgstr "Seçilmişlərdə göstərmək"

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgid "Icon:"
msgstr "İkon:"

#: package/contents/ui/ConfigGeneral.qml:126
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Seçim…"

#: package/contents/ui/ConfigGeneral.qml:131
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "İkonu silmək"

#: package/contents/ui/ConfigGeneral.qml:149
#, kde-format
msgid "Show applications as:"
msgstr "Tətbiqlərin adlandırma forması:"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name only"
msgstr "Yalnız adı"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description only"
msgstr "Yalnız təsviri"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Name (Description)"
msgstr "Ad (Təsviri)"

#: package/contents/ui/ConfigGeneral.qml:151
#, kde-format
msgid "Description (Name)"
msgstr "Təsviri (Adı)"

#: package/contents/ui/ConfigGeneral.qml:161
#, kde-format
msgid "Behavior:"
msgstr "Davranış:"

#: package/contents/ui/ConfigGeneral.qml:163
#, kde-format
msgid "Sort applications alphabetically"
msgstr "Tətbiqləri əlifba sırası ilə çeşidləmək"

#: package/contents/ui/ConfigGeneral.qml:171
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr "Alt menyuları bir səviyyədə düzmək"

#: package/contents/ui/ConfigGeneral.qml:179
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr "Yuxarı səviyyədəki elementlərin nişanlarını göstərmək"

#: package/contents/ui/ConfigGeneral.qml:189
#, kde-format
msgid "Show categories:"
msgstr "Kateqoriyaları göstərmək:"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Recent applications"
msgstr "Sonuncu tətbiqlər"

#: package/contents/ui/ConfigGeneral.qml:193
#, kde-format
msgid "Often used applications"
msgstr "Tez-tez istifadə olunan tətbiqlər"

#: package/contents/ui/ConfigGeneral.qml:200
#, kde-format
msgid "Recent files"
msgstr "Sonuncu fayllar"

#: package/contents/ui/ConfigGeneral.qml:201
#, kde-format
msgid "Often used files"
msgstr "Tez-tez istifadə olunan fayllar"

#: package/contents/ui/ConfigGeneral.qml:208
#, kde-format
msgid "Recent contacts"
msgstr "Sonuncu əlaqələr"

#: package/contents/ui/ConfigGeneral.qml:209
#, kde-format
msgid "Often used contacts"
msgstr "Tez-tez istifadə olunan əlaqələri göstərmək"

#: package/contents/ui/ConfigGeneral.qml:215
#, kde-format
msgid "Sort items in categories by:"
msgstr "Kateqoriyadakı elementlərin çeşidlənməsi:"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "Son istifadə olunanlar"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "Tez-tez istifadə olunanlar"

#: package/contents/ui/ConfigGeneral.qml:226
#, kde-format
msgid "Search:"
msgstr "Axtarış:"

#: package/contents/ui/ConfigGeneral.qml:228
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Əlfəcinlər, fayllar və e-poçtlar arasında axtarmaq"

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Align search results to bottom"
msgstr "Axtarış nəticələrini aşağıda yerləşdirmək"

#: package/contents/ui/DashboardRepresentation.qml:292
#, kde-format
msgid "Searching for '%1'"
msgstr "\"%1\" axtarışı"

#: package/contents/ui/DashboardRepresentation.qml:292
#, kde-format
msgid "Type to search…"
msgstr "Axtarış sözünü daxil edin"

#: package/contents/ui/DashboardRepresentation.qml:392
#, kde-format
msgid "Favorites"
msgstr "Seçilmişlər"

#: package/contents/ui/DashboardRepresentation.qml:610
#: package/contents/ui/DashboardTabBar.qml:42
#, kde-format
msgid "Widgets"
msgstr "Vidjetlər"

#: package/contents/ui/DashboardTabBar.qml:31
#, kde-format
msgid "Apps & Docs"
msgstr "Tətbiqlər və sənədlər"

#: package/contents/ui/main.qml:259
#, kde-format
msgid "Edit Applications…"
msgstr "Tətbiqər siyahısına düzəliş…"

#~ msgid "Search…"
#~ msgstr "Axtarış…"

#~ msgid "Search..."
#~ msgstr "Axtarış..."

#~ msgid "Often used documents"
#~ msgstr "Tez-tez istifadə olunan sənədlər"
